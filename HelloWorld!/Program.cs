﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_world
{
    class Program
    {
        static int age = 16;
        static string myage = "My Age is: ";
        static float exactage = 16.25f;
        static List<int> iList = new List<int>();



        static void Main(string[] args)
        {
            Console.WriteLine(myage + age);
            Console.WriteLine("My exact age is: " + exactage);
            iList.Add(2);
            iList.Add(3);
            iList.Add(5);
            iList.Add(7);
            iList.Add(6);
            Console.WriteLine("The numbers in my list are: " + iList[0] + iList[1] + iList[2] + iList[3] + iList[4]);

            Console.ReadKey();
        }
    }
}